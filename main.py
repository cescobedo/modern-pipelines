import os
import copy
from typing import TypeVar

from prefect import flow, get_run_logger

from src.lib.configuration import Task, AbstractPipelineConfiguration
from src.lib.factory import storage_factory, pipeline_config_factory
from src.tasks import task_factory
import polars as pl

"""
storage = create_storage("gcs")(
    "data-bucket",
    client_options={"api_endpoint": "http://0.0.0.0:5050"},
    use_auth_w_custom_endpoint=False,
)
"""

T = TypeVar("T")

df = pl.DataFrame(
    [
        {"name": "Alice", "age": 25, "city": "New York"},
        {"name": "Bob", "age": 30, "city": "Los Angeles"},
        {"name": "Charlie", "age": 28, "city": "Chicago"},
        {"name": "David", "age": 35, "city": "San Francisco"},
        {"name": "Eve", "age": 22, "city": "Miami"},
    ]
)


@flow
def apply_tasks(data: T, tasks: list[Task]) -> T:
    logger = get_run_logger()
    logger.info(f"applying {tasks} tasks to {data}")
    _data = copy.deepcopy(data)
    for task in tasks:
        _data = task_factory.get(task.name)(_data, **task.parameters)
    return _data


@flow
def get_pipeline_config(
    storage_type: str, config_type: str
) -> AbstractPipelineConfiguration:
    logger = get_run_logger()
    logger.info(f"loading pipeline config from {config_type} storage")
    base_path = os.path.join(os.getcwd(), "cloud-storage/data-bucket")
    storage = storage_factory.get(storage_type)(base_path)
    return pipeline_config_factory.get(config_type)(storage)


@flow
def my_flow(source_name: str, task_type: str):
    logger = get_run_logger()
    pipeline_config = get_pipeline_config("fs", "yaml")
    tasks = pipeline_config.get_tasks(source_name, task_type)
    result = apply_tasks(df, tasks)
    logger.info(result)


if __name__ == "__main__":
    my_flow("in-memory-dataframe", "transformation")
