import polars as pl
from polars import DataFrame

from prefect import task, get_run_logger
from . import task_factory


@task_factory.register("df.sql")
@task
def sql(data: DataFrame, query: str) -> DataFrame:
    logger = get_run_logger()
    logger.info(f"executing query: \n{query}")
    res = pl.SQLContext(frame=data).execute(query)
    return res.collect()


@task_factory.register("df.expression")
@task
def expression(_: DataFrame, expr: str) -> DataFrame:
    logger = get_run_logger()
    logger.info(f"executing expression: \n{expr}")
    res = eval(expr)
    return res
