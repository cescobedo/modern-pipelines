import os
from abc import ABC, abstractmethod
from typing import Any, AnyStr
from ..lib.factory import storage_factory

from google.cloud import storage
from google.cloud.storage import Blob


class AbstractStorage(ABC):
    @abstractmethod
    def write(self, key, data):
        pass

    @abstractmethod
    def read(self, key) -> Any:
        pass


@storage_factory.register("gcs")
class GCSStorage(AbstractStorage):
    def __init__(self, bucket_name: str, **kwargs):
        self.gcs = storage.Client(**kwargs)
        self.bucket = self.gcs.bucket(bucket_name)

    def write(self, key: str, data: AnyStr) -> None:
        blob: Blob = self.bucket.blob(key)
        blob.upload_from_string(data)

    def read(self, key: str) -> bytes:
        blob: Blob = self.bucket.blob(key)
        return blob.download_as_bytes()


@storage_factory.register("fs")
class FileSystemStorage(AbstractStorage):
    def __init__(self, path: str):
        self.path = path

    def write(self, key: str, data: AnyStr) -> None:
        if isinstance(data, bytes):
            mode = "wb"
        elif isinstance(data, str):
            mode = "w"
        else:
            raise ValueError(f"Unsupported data type: {type(data)}")

        with open(os.path.join(self.path, key), mode) as f:
            f.write(data)

    def read(self, key: str) -> bytes:
        with open(os.path.join(self.path, key), "rb") as f:
            return f.read()
