from typing import TypeVar
from pydantic import BaseModel, PrivateAttr


T = TypeVar("T")


class NotRegisteredError(RuntimeError):
    pass


class Factory(BaseModel):
    name: str
    _registry: dict[str, T] = PrivateAttr(default={})

    def register(self, name: str) -> T:
        def wrapper(wrapped: T) -> T:
            self._registry[name] = wrapped
            return wrapped

        return wrapper

    def get(self, name: str) -> T:
        obj = self._registry.get(name)
        if obj is None:
            raise NotRegisteredError(f"'{name}' is not registered in {self.name}")
        return obj


storage_factory = Factory(name="StorageFactory")
pipeline_config_factory = Factory(name="PipelineConfigFactory")
task_factory = Factory(name="TaskFactory")
