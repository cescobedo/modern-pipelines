import json
import yaml
from abc import ABC, abstractmethod
from typing import Any
from pydantic import BaseModel
from ..lib.storage import AbstractStorage
from ..lib.factory import pipeline_config_factory


class Task(BaseModel):
    name: str
    type: str
    priority: int
    parameters: dict = {}


class Source(BaseModel):
    name: str
    tasks: list[Task]


class AbstractPipelineConfiguration(ABC, BaseModel):
    version: str
    sources: list[Source]

    @staticmethod
    @abstractmethod
    def _load(storage: AbstractStorage, key: str) -> Any:
        pass

    def get_tasks(self, source_name: str, task_type: str) -> list[Task]:
        return sorted(
            [
                task
                for source in self.sources
                for task in source.tasks
                if source.name == source_name and task.type == task_type
            ],
            key=lambda task: task.priority,
        )


@pipeline_config_factory.register("yaml")
class YamlPipelineConfiguration(AbstractPipelineConfiguration):
    def __init__(self, storage: AbstractStorage, key: str = "pipeline.yaml"):
        configuration = self._load(storage, key)
        super().__init__(**configuration)

    @staticmethod
    def _load(storage: AbstractStorage, key: str) -> Any:
        configuration = storage.read(key)
        return yaml.safe_load(configuration)


@pipeline_config_factory.register("json")
class JsonPipelineConfiguration(AbstractPipelineConfiguration):
    def __init__(self, storage: AbstractStorage, key: str = "pipeline.json"):
        configuration = self._load(storage, key)
        super().__init__(**configuration)

    @staticmethod
    def _load(storage: AbstractStorage, key: str) -> Any:
        configuration = storage.read(key)
        return json.loads(configuration)
