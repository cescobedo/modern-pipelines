from pytest import fixture
from prefect.logging.loggers import disable_run_logger
from prefect.testing.utilities import prefect_test_harness


@fixture(autouse=True, scope="session")
def prefect_test_fixture() -> None:
    with prefect_test_harness():
        yield


@fixture(autouse=True, scope="session")
def prefect_disable_run_logging() -> None:
    """Disable prefect logging error: prefect.exceptions.MissingContextError.

    Without this fixture an exception is raised by the prefect logging system:
    `MissingContextError: There is no active flow or task run context.`
    """
    with disable_run_logger():
        yield
