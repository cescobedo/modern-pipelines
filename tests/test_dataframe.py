from src.tasks import task_factory
from polars import DataFrame


class TestDataFrameTransformations:
    data = DataFrame(
        [
            {"name": "Alice", "age": 25, "city": "New York"},
            {"name": "Bob", "age": 30, "city": "Los Angeles"},
            {"name": "Charlie", "age": 28, "city": "Chicago"},
            {"name": "David", "age": 35, "city": "San Francisco"},
            {"name": "Eve", "age": 22, "city": "Miami"},
        ]
    )

    def test_sql_task(self):
        query = "SELECT * FROM frame WHERE age >= 30"
        expected_result = DataFrame(
            [
                {"name": "Bob", "age": 30, "city": "Los Angeles"},
                {"name": "David", "age": 35, "city": "San Francisco"},
            ]
        )

        task_run_result = task_factory.get("df.sql")(self.data, query)
        assert expected_result.equals(task_run_result)

    def test_expression_task(self):
        expr = "_.filter(pl.col('age') >= 30)"
        expected_result = DataFrame(
            [
                {"name": "Bob", "age": 30, "city": "Los Angeles"},
                {"name": "David", "age": 35, "city": "San Francisco"},
            ]
        )

        task_run_result = task_factory.get("df.expression")(self.data, expr)
        assert expected_result.equals(task_run_result)
